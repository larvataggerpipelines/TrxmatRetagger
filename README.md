# Deployment of LarvaTagger for trx.mat data

This "pipeline" processes tracking data in the *trx.mat* file format (files generated using [JB's tagger](https://github.com/DecBayComp/Pipeline_action_analysis_t5_pasteur_janelia) described in [Masson *et al.* 2020](https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1008589)).
It ignores the tagging information and instead runs the MaggotUBA-based [20230311 tagger](https://gitlab.pasteur.fr/nyx/MaggotUBA-adapter#20230311-0-and-20230311) on the stored spines.

Along with each original *trx.mat* file, a *predicted.20230311.label* file is generated and an additional *trx.20230311.mat* file is created from a copy of the original *trx.mat* file with behavior labels overwritten using the newly predicted labels.

The *.label* to *.mat* conversion step requires Matlab or the Matlab Runtime Engine (mcr). On Maestro, Matlab is available and the conversion code is compiled using `mcc`. The resulting binary was made publicly available as a downloadable artifact so that, on other HPC clusters, the published binary is downloaded instead and only mcr is loaded. mcr does not require any paid license.

The pipeline currently does not run JB's tagger, and instead relies on already processed data repositories.

## Usage

On any of the supported HPC clusters, general usage is:

```
git clone https://gitlab.com/larvataggerpipelines/TrxmatRetagger
cd TrxmatRetagger
make all
```

To process a specific tracker or data repository, say *t1*:
```
make TRACKER=t1 all
```

`make all` submits Slurm jobs whose status can be inspected with `squeue --me`. If the latter command prints more than a header line, not all jobs are complete yet.
Jobs flush their log messages in files in the *log* directory.

The running and pending jobs can be stopped with `scancel --me` (or `scancel -u $USER` on *hex*).

Other `make` targets are available. For example, to clear most intermediate files, including log files:
```
make clean
```

`make all` can be run multiple times. It will only process the data files that are not processed yet.

## Tweaks

### Output file location

For all the supported platforms, the generated files are stored in a new repository following a similar directory structure to the input repository. If these repositories are distinct, the copies of the original *trx.mat* files are removed so that each leaf directory only contains the *.label* and modified *.mat* files.

It is possible - although this has not been tested - to make the pipeline store the new files in the original repository instead. This is controlled by the `PROCESSED_DATA` variable at the top of the *Makefile.&ast;* file for your platform (see the [Supported HPC clusters](#supported-hpc-clusters) section below).
This `PROCESSED_DATA` variable can be set equal to the `RAW_DATA` variable.

### Default trackers

The platform-specific *Makefile.&ast;* file lists default trackers with the `TRACKERS` variable, at the top of the file. Feel free to modify the list in the file.

Note that a tracker does not have to be listed to be processed with the explicit `make TRACKER=<tracker> all` command.

### Soft termination

The pipeline include several processing steps. The last one simply fixes file permissions and ownership, so that any member of the same Unix group (basically any lab member) can modify or delete the generated files.

To terminate a series of jobs before they are all done, and still allow the subsequent postprocessing step to run, it is preferable to cancel the pending jobs only with:
```
scancel --state=PENDING <jobid>
```
or - simpler, if you did not submit any other job and only ran the *TrxmatRetagger* pipeline, with `scancel --state=PENDING --me`.

The job id is printed by the `squeue --me` command. It appears at the beginning of each line, and is the numeric part before the first *_* character. The numeric part after the *_* character is a task id.
For example, in the following output:
```
            JOBID     USER              ACCOUNT           NAME  ST  TIME_LEFT NODES CPUS TRES_PER_N MIN_MEM NODELIST (REASON)
      ...
      5364780_210 flaurent        def-tolab_cpu t94-larvatagge   R    4:41:00     1    8        N/A      8G gra1100 (None)
      5364780_211 flaurent        def-tolab_cpu t94-larvatagge   R    4:41:06     1    8        N/A      8G gra394 (None)
5364780_[212-438% flaurent        def-tolab_cpu t94-larvatagge  PD    5:00:00     1    8        N/A      8G  (JobArrayTaskLimit)
```
the job id is 5364780.

Note that multiple jobs may appear. Indeed, `make all` submits as many jobs (more exactly job arrays) as trackers.

Also note it may take the running jobs a while to complete and step down in favour of the postprocessing step.

Alternatively, the postprocessing step can be run with:
```
chmod a+x bin/postprocess.sh
srun bin/postprocess.sh data/processed/<tracker>
```

## Supported HPC clusters

### Maestro

Maestro is the main HPC cluster at Institut Pasteur, Paris.

The pipeline is designed to process data from Tihana Jovanic's lab.

The user is assumed to be part of the *cifs-Hecatonchire* Unix group and have access to the *dbc_pmo* Slurm partition.

See also the *Makefile.maestro* file.

### Graham

Graham is one of the HPC clusters operated by the Digital Research Alliance of Canada.

The pipeline is designed to process data from Tomoko Ohyama's lab.

The user is assumed to be part of the *def-tolab* Unix group.

See also the *Makefile.graham* file.

### MRC LMB's cluster

Jobs can be submitted from the *hex* node.

The pipeline is designed to process data from Marta Zlatic's lab.

The user is assumed to be part of the *zlaticlab* Unix group.

See also the *Makefile.lmb* file.

## Known issues

The *bin/diagnose.sh* script may help to identify common errors. It greps the possibly many log files in search for specific pieces of message, and prints the lines that contain these messages, along with the corresponding log file names.

### Job submission limits

On Graham or the EMBL Heidelberg's cluster, the number of submitted jobs is bounded to 1,000.

`make all` or `make submit` partially manage this limit capping the number of jobs at 1,000 and telling the user to run the same command again once all jobs are complete.
However, these commands do so on a per-tracker basis. If all trackers are processed simultaneously (default), the following message may appear at the submission step:

```
sbatch: error: AssocMaxSubmitJobLimit
sbatch: error: Batch job submission failed: Job violates accounting/QOS policy (job submit limit, user's size and/or time limits)
```

Not all jobs will run and, as a consequence, not all the data files will be processed.
Running the command multiple times (each time once all jobs are complete) should eventually make all the data files processed.

Alternatively, you can try to process one tracker at a time, with `make TRACKER=t1 all` with hypothetical tracker t1 as an example.

### Out-of-memory errors

Memory requirements per job are hard-coded in the main *Makefile* file.
If a data file requires more memory than requested, the job is killed and the corresponding log file will display a message similar to the following:

```
slurmstepd: error: Detected 1 oom-kill event(s) in [...]. Some of your processes may have been killed by the cgroup out-of-memory handler.
srun: error: [...]: task 0: Out Of Memory
```

Edit the *Makefile* file to increase the value of the `MEMORY` variable, at the top of the file.

After editing any file, be sure to run `make clean`.

### Matlab's component cache is corrupt

The log messages may display:
```
The component cache extracted by the application is corrupt. Contact MathWorks Support for assistance.
 Details: 'Extracted component cache is not complete.'
 Original component cache dir is copied to '/[...]' .
```

The issue is harmless. A *.mcrCache9.11* hidden directory is created in the user's home directory to stash a number of directories and files that can be ignored.

A postprocessing step purges all *~/.mcrCache&ast;* directories.

### Time limits

On the hosts for which a time limit is set, some jobs may prematurely terminate with a message similar to the following:
```
slurmstepd: error: *** JOB [...] ON [...] CANCELLED AT [...] DUE TO TIME LIMIT ***
slurmstepd: error: *** STEP [...] ON [...] CANCELLED AT [...] DUE TO TIME LIMIT ***
```
This seems to be caused by the job hanging for an unknown reason. As the data repositories are stored on network partitions, temporary downtime is likely to result in a job operation to indefinitely wait.

As a consequence, since a job is programmed to process multiple data files, some data files may not be processed.

This can (hopefully) be fixed launching the pipeline again once all jobs are complete.

### Disk quota exceeded

On Graham, use the `quota` command to check how many files there are in the shared partition (line `/project (group def-tolab)`).
The quota that is most commonly reached is on the number of files, rather than the disk space used.

To check how many files the present pipeline has created, do:
```
find $(realpath data/processed) -type f | wc -l
```
It is unlikely the printed number will represent any significant fraction of the quota.
Other directories might contain far more files.
