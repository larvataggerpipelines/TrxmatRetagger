#!/usr/bin/env bash

if [ -d log ]; then
  grep 'Disk quota exceeded' log/slurm*.out
  grep 'Out Of Memory' log/slurm*.out
  grep 'DUE TO TIME LIMIT' log/slurm*.out
else
  echo 'No log files found'
fi
