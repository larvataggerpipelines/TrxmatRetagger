#!/bin/bash

if [ -z "$(which sacctmgr)" ]; then
  hostname
elif [ -n "$(sacctmgr list cluster | tail -n+3 | grep graham)" ]; then
  echo graham
elif [ -n "$(sacctmgr list cluster | tail -n+3 | grep maestro)" ]; then
  echo maestro
elif [ -n "$(sacctmgr list cluster | tail -n+3 | grep cluster)" ]; then
  if [ "$(hostname | cut -d. -f2)" = lmb ]; then
    hostname | cut -d. -f1-2
  else
    hostname
  fi
else
  hostname
fi
