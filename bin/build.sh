#!/usr/bin/env bash

if [ -z "$MATLAB_VERSION" ]; then
  MATLAB_VERSION=$1
fi

module load matlab/R$MATLAB_VERSION
rm -rf PlanarLarvae
CLONE="git clone -n https://gitlab.pasteur.fr/nyx/planarlarvae.jl PlanarLarvae"
if ! [ $(eval $CLONE) ]; then
  if [ "$(git config --global http.sslverify)" = true ]; then
    git config --global http.sslverify false
    eval $CLONE
    git config --global http.sslverify true
  fi
fi
cd PlanarLarvae && git checkout 72b4a57a
cd nyxlabels && mcc -m -v run_update_trx_with_labels.m
cd ../..
mv PlanarLarvae/nyxlabels/run_update_trx_with_labels update_trx_with_labels_$MATLAB_VERSION
rm -rf PlanarLarvae
