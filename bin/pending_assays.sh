#!/bin/bash

if [ -z "$1" ]; then
  TAGGER=
else
  TAGGER=".$1"
fi
if [ -z "$2" ]; then
  DIR=.
else
  DIR=$2
fi

(cd data/raw; find ${DIR} -name trx.mat -print0) | xargs -0 -I % sh -c "if ! [ -f \"data/processed/\$(dirname %)/trx${TAGGER}.mat\" ]; then echo %; fi"
