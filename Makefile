SHELL:=/bin/bash
# default values
MATLAB_VERSION:=2021b
TAGGER:=20230311
LARVATAGGER_IMAGE:=flaur/larvatagger:0.13.1-$(TAGGER)
APPTAINER:=apptainer
APPTAINER_VERSION:=1.1.6
SRUN:=srun
CPUS_PER_TASK:=8
MEMORY:=8G
ASSAYS_PER_JOB:=10

TRACKER:=
HOST:=$(shell bin/host.sh)

export SHELL MATLAB_VERSION TAGGER LARVATAGGER_IMAGE APPTAINER_VERSION SRUN CPUS_PER_TASK

ifeq ($(HOST),maestro)
	include Makefile.maestro
endif

ifeq ($(HOST),graham)
	include Makefile.graham
endif

ifeq ($(HOST),hex.lmb)
	export HOST:=hex
	include Makefile.lmb
endif

ifeq ($(HOST),hal.lmb)
	export HOST:=hal
	include Makefile.lmb
endif

.PHONY: fallback build clean deepclean silentclean update jobs submit all .force $(TRACKERS)

fallback:
	@echo Host not supported: $(HOST)

SUBMIT_SCRIPTS:=$(patsubst %,bin/%/submit.sh,$(TRACKERS))
JOB_SCRIPTS:=$(patsubst %,bin/%/job.sh,$(TRACKERS))
.ONESHELL: bin/larvatagger.sif $(SUBMIT_SCRIPTS)

ifdef TRACKER
.PRECIOUS: data/interim/$(TRACKER)/pending_assays.txt
else
ASSAY_LISTS:=$(patsubst %,data/interim/%/pending_assays.txt,$(TRACKERS))
.PRECIOUS: $(ASSAY_LISTS)
endif

build: bin/larvatagger.sif bin/update_trx_with_labels_$(MATLAB_VERSION) bin/mcrpath.sh bin/postprocess.sh

bin/larvatagger.sif:
	module load apptainer/$(APPTAINER_VERSION)
	# docker:// urls do not work on worker nodes (e.g. using srun):
	@echo 'vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv Building Apptainer image vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv'
	apptainer build bin/larvatagger.sif docker://$(LARVATAGGER_IMAGE)
	@echo '∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧ Apptainer image built ∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧∧'

data: data/interim data/raw data/processed
# interim comes first because it creates the `data` directory
data/interim:
	mkdir -p data/interim
data/raw:
	ln -s $(RAW_DATA) data/raw
data/processed: $(PROCESSED_DATA)
	ln -Ts $(PROCESSED_DATA) data/processed
ifdef PROCESSED_DATA
$(PROCESSED_DATA):
	mkdir -p $(PROCESSED_DATA)
	chmod g+rwX $(PROCESSED_DATA)
endif

data/interim/%/pending_assays.txt: data .force
	mkdir -p data/interim/$*
	$(SRUN) -J $*-list bin/pending_assays.sh $(TAGGER) $* > $@

deepclean:
	@$(MAKE) silentclean
	rm -f bin/larvatagger.sif
	rm -f bin/update_trx_with_labels_*
	rm -rf bin/matlab

clean:
	@$(MAKE) silentclean
	@echo 'Run target `deepclean` instead of `clean` if any of `MATLAB_VERSION`, `TAGGER` or `LARVATAGGER_IMAGE` has changed'

silentclean:
	rm -rf data
	rm -rf bin/t[1-9]*
	# not all trackers begin with "t"
	for TRACKER in $(TRACKERS); do \
		rm -rf bin/$$TRACKER; \
	done
	rm -f bin/mcrpath.sh bin/postprocess.sh
	rm -rf log

update: deepclean
	git stash
	git pull

all: jobs submit

ifdef TRACKER
submit: build
	if [ -s data/interim/$(TRACKER)/pending_assays.txt ]; then \
		mkdir -p log; \
		OUT=$$(sbatch bin/$(TRACKER)/submit.sh); \
		echo $$OUT; \
		if [ -f bin/postprocess.sh ] && [[ "$$OUT" =~ 'Submitted batch job ([0-9]+)' ]]; then \
			JOBID=$${BASH_REMATCH[1]}; \
			sbatch -J $(TRACKER)-post -o /dev/null --depend=afterany:$$JOBID bin/postprocess.sh $$(realpath data/processed/$(TRACKER)); \
		fi; \
	else \
		echo "$(TRACKER): no more pending assays"; \
	fi
jobs:
	rm -rf bin/$(TRACKER)
	mkdir -p bin/$(TRACKER)
	@$(MAKE) bin/$(TRACKER)/submit.sh bin/$(TRACKER)/job.sh
else
submit: build
	for TRACKER in $(TRACKERS); do \
		$(MAKE) TRACKER=$$TRACKER submit; \
	done
	@squeue -u $$USER
jobs:
	for TRACKER in $(TRACKERS); do \
		$(MAKE) TRACKER=$$TRACKER jobs; \
	done
endif


bin/%/submit.sh: data/interim/%/pending_assays.txt
	NASSAYS=$$(wc -l data/interim/$*/pending_assays.txt | cut -d\  -f1)
	NJOBS=$$((NASSAYS / $(ASSAYS_PER_JOB) + 1))
	if (( $$NJOBS > 1000 )); then \
		echo "Maximum number of submitted jobs reached: $$NJOBS > 1000;"; \
		echo "please run \`make all\` again once all jobs are complete"; \
		NJOBS=1000; \
	fi
	mkdir -p bin/$*
	sed -e "s/%TRACKER%/$*/g" \
		-e "s/%NJOBS%/$$NJOBS/g" \
		-e "s/%MEMORY%/$(MEMORY)/g" \
		-e "s/%CPUS_PER_TASK%/$(CPUS_PER_TASK)/g" \
		-e "s/%APPTAINER_VERSION%/$(APPTAINER_VERSION)/g" \
		-e "s/%SBATCH_EXTRA_SHEBANG%/$(SBATCH_EXTRA_SHEBANG)/g" \
		bin/submit.template > $@

bin/%/job.sh: .force
	mkdir -p bin/$*
	sed -e "s/%TRACKER%/$*/g" \
		-e "s/%TAGGER%/$(TAGGER)/g" \
		-e "s/%CPUS_PER_TASK%/$(CPUS_PER_TASK)/g" \
		-e "s/%ASSAYS_PER_JOB%/$(ASSAYS_PER_JOB)/g" \
		-e "s/%MATLAB_VERSION%/$(MATLAB_VERSION)/g" \
		bin/job.template > $@
	chmod a+x $@

.force:
